all:
	g++ -std=c++17 main.cpp -o hello
	g++ -std=c++17 mainTest.cpp -lcppunit -o helloTest

test:
	chmod +x hello
	./helloTest

clean:
	rm -rf hello helloTest